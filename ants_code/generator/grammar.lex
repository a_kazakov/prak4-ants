%{
    #define YYSTYPE std::string
    #include <string>
    #include "grammar.tab.h"
    extern int yylineno;
%}

%option noyywrap

%%

if {
    return T_IF;
}
else {
    return T_ELSE;
}
while {
    return T_WHILE;
}
put_smell {
    return T_PUT_SMELL;
}
\$\$ {
    return T_2BUCKS;
}
hasFood {
    yylval = yytext;
    return T_CONST;
}
(move_(up|down|left|right)|bite_(up|down|left|right)|put|get) {
    yylval = yytext;
    return T_SPECIAL_CMD;
}
[0-9]+ {
    yylval = yytext;
    return T_INT;
}
[a-zA-Z_][a-zA-Z_0-9]* {
    yylval = yytext;
    return T_WORD;
}
== {
    return T_EQUAL;
}
[ \r\t]

\n {
    ++yylineno;
}

. {
    return *yytext;
}

%%
