%{
    #include <iostream>
    #include <string>
    int yylex();
    void yyerror(const char *);
    std::string makeLabel();
    std::string encode_str(std::string &s);
    #define YYSTYPE std::string
%}

%%

%token T_PUT_SMELL;
%token T_INT T_WORD;
%token T_IF T_ELSE T_WHILE T_SPECIAL_CMD;
%token T_2BUCKS T_CONST;
%left '&' '|';
%left '<' '>' T_EQUAL;
%left '+' '-';
%left '*' '/' '%';
%left '!';
%left NEG;

res: 
    code {
        std::cout << encode_str($1);
    }
;
    
code
    : { $$ = ""; }
    | code cmd {
        $$ = $1 + $2;
    }
;

cmd
    : assignment
    | if_cmd
    | while_cmd
    | special_cmd
    | put_smell_cmd
;

sensor_data:
    '#' T_INT '.' T_WORD {
        $$ = "%" + $4 + "\n" + $2.substr(0, 1) + "\n" + $2.substr(1, 1) + "\n#\n";
    }
;

memory_data:
    T_2BUCKS '[' exp ']' {
        $$ = $3 + "$$\n";
    }
;

var
    : '$' T_WORD {
        $$ = 
        $$ = "%" + $2 + "\n$\n";
    }
;

exp
    : var {
        $$ = $1 + "^\n";
    }
    | T_INT {
        $$ = $1 + "\n";
    }
    | T_CONST {
        $$ = "%" + $1 + "\n@\n";
    }
    | sensor_data
    | memory_data {
        $$ = $1 + "^\n";
    }
    | exp '+' exp {
        $$ = $1 + $3 + "+\n";
    }
    | exp '-' exp {
        $$ = $1 + $3 + "-\n";
    }
    | exp '*' exp {
        $$ = $1 + $3 + "*\n";
    }
    | exp '/' exp {
        $$ = $1 + $3 + "/\n";
    }
    | exp '%' exp {
        $$ = $1 + $3 + "%\n";
    }
    | exp '&' exp {
        $$ = $1 + $3 + "&\n";
    }
    | exp '|' exp {
        $$ = $1 + $3 + "|\n";
    }
    | exp '<' exp {
        $$ = $1 + $3 + "<\n";
    }
    | exp '>' exp {
        $$ = $1 + $3 + ">\n";
    }
    | exp T_EQUAL exp {
        $$ = $1 + $3 + "==\n";
    }
    | '-' exp %prec NEG {
        $$ = $2 + "n\n";
    }
    | '(' exp ')' {
        $$ = $2;
    }
    | '!' exp {
        $$ = $2 + "!\n";
    }
;

assignment
    : var '=' exp ';' {
        $$ = $1 + $3 + "=\n;\n";
    }
    | memory_data '=' exp ';' {
        $$ = $1 + $3 + "=\n;\n";
    }
;

codeblock
    : '{' code '}' {
        $$ = $2;
    }
;

if_cmd
    : T_IF exp codeblock {
        std::string l1 = makeLabel();
        $$ = $2 + "!\n%" + l1 + "\nif\n" + $3 + ":" + l1 + "\n";
    }
    | T_IF exp codeblock T_ELSE codeblock {
        std::string l1 = makeLabel();
        std::string l2 = makeLabel();
        $$ = $2 + "!\n" + "%" + l1 + "\nif\n" + $3 + "%" + l2 + "\ngoto\n:" + l1 + "\n" + $5 + ":" + l2 + "\n";
    }
;

while_cmd
    : T_WHILE exp codeblock {
        std::string l1 = makeLabel();
        std::string l2 = makeLabel();
        $$ = ":" + l2 + "\n" + $2 + "!\n" + l1 + "if\n" + $3 + ":" + l1 + "\n" + l2 + "\ngoto\n";
    }
;

special_cmd
    : T_SPECIAL_CMD ';' {
        $$ = $1 + "\n";
    }
;

put_smell_cmd
    : T_PUT_SMELL exp ';' {
        $$ = $2 + "put_smell\n";
    }
;

%%
