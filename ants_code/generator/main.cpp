#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

int curlineno = 0;

int yyerror(char const *x) {
    extern int yylineno;
    curlineno = yylineno;
    throw std::runtime_error(x);
}

void yyparse();

std::string decode_str(std::string &s);

std::string encode_smile(unsigned char x) {
    int p1 = x >> 7;
    int p2 = (x & 0x70) >> 4;
    int p3 = (x & 0x08) >> 3;
    int p4 = (x & 0x07);
    char res[6] = {"(["[p1], "oO*^@0$-"[p2], "_."[p3], "oO*^@0$-"[p4], ")]"[p1], '\0' };
    return res;
}

std::string encode_str(std::string &s) {
    std::string r;
    for (size_t i = 0; i < s.length(); ++i) {
        r += encode_smile(s[i]);
        if (i % 16 == 15) {
            r += "\n";
        }
    }
    return r;
}

char decode_smile(char x1, char x2, char x3, char x4) {
    unsigned char res = 0;
    switch (x1) {
        case '(': res |= 0x00; break;
        case '[': res |= 0x80; break;
    }
    switch (x2) {
        case 'o': res |= 0x00; break;
        case 'O': res |= 0x10; break;
        case '*': res |= 0x20; break;
        case '^': res |= 0x30; break;
        case '@': res |= 0x40; break;
        case '0': res |= 0x50; break;
        case '$': res |= 0x60; break;
        case '-': res |= 0x70; break;
    }
    switch (x3) {
        case '_': res |= 0x00; break;
        case '.': res |= 0x08; break;
    }
    switch (x4) {
        case 'o': res |= 0x00; break;
        case 'O': res |= 0x01; break;
        case '*': res |= 0x02; break;
        case '^': res |= 0x03; break;
        case '@': res |= 0x04; break;
        case '0': res |= 0x05; break;
        case '$': res |= 0x06; break;
        case '-': res |= 0x07; break;
    }
    //std::cerr << x1 << " " << x2 << " " << x3 << " " << x4 << " " << int(res) << " " << res << std::endl;
    return res;
}

std::string decode_str(std::string &s) {
    char r[s.length() / 5 + 1];
    int k = 0;
    for (size_t i = 0; i < s.length() - 4; i += 5) {
        r[k++] = decode_smile(s[i], s[i + 1], s[i + 2], s[i + 3]);
        if (k % 16 == 0) {
            ++i;
        }
    }
    r[k] = '\0';
    return r;
}

std::string makeLabel() {
    static int cnt = 0;
    std::stringstream ss;
    ss << "L" << (++cnt);
    return ss.str();
}

int main(int argc, char *argv[]) {
    try {
        yyparse();
    } catch (std::exception &x) {
        std::cerr << x.what() << " at line " << curlineno << std::endl;
        return -1;
    }
    catch (...) {
        std::cerr << "Unknown error." << std::endl;
        return -1;
    }
    std::cout << std::endl;
    return 0;
}
