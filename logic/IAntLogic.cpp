#include "IAntLogic.hpp"

#include "IAntLogicImpl.hpp"

#include "Language.hpp"

#include <iostream>
#include <fstream>

namespace antlogic
{
    std::shared_ptr<IAntLogic> IAntLogic::GetAntLogic(int teamId)
    {
        std::ifstream f;
        switch (teamId) {
        case 0:
            if (f.open("ants_code/ant1.pp"), f.is_open()) {
                return std::make_shared<Executer>("ants_code/ant1.pp");
            } else {
                return std::make_shared<AntLogicTeam01>();
            }
        case 1:
            if (f.open("ants_code/ant2.pp"), f.is_open()) {
                return std::make_shared<Executer>("ants_code/ant2.pp");
            } else {
                return std::make_shared<AntLogicTeam02>();
            }
        case 2:
            if (f.open("ants_code/ant3.pp"), f.is_open()) {
                return std::make_shared<Executer>("ants_code/ant3.pp");
            } else {
                return std::make_shared<AntLogicTeam01>();
            }
        default:
            if (f.open("ants_code/ant4.pp"), f.is_open()) {
                return std::make_shared<Executer>("ants_code/ant4.pp");
            } else {
                return std::make_shared<AntLogicTeam03>();
            }
        }
    }
}

