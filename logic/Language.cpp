#include "Language.hpp"

namespace antlogic {
    char decode_smile(char x1, char x2, char x3, char x4) {
        unsigned char res = 0;
        switch (x1) {
            case '(': res |= 0x00; break;
            case '[': res |= 0x80; break;
        }
        switch (x2) {
            case 'o': res |= 0x00; break;
            case 'O': res |= 0x10; break;
            case '*': res |= 0x20; break;
            case '^': res |= 0x30; break;
            case '@': res |= 0x40; break;
            case '0': res |= 0x50; break;
            case '$': res |= 0x60; break;
            case '-': res |= 0x70; break;
        }
        switch (x3) {
            case '_': res |= 0x00; break;
            case '.': res |= 0x08; break;
        }
        switch (x4) {
            case 'o': res |= 0x00; break;
            case 'O': res |= 0x01; break;
            case '*': res |= 0x02; break;
            case '^': res |= 0x03; break;
            case '@': res |= 0x04; break;
            case '0': res |= 0x05; break;
            case '$': res |= 0x06; break;
            case '-': res |= 0x07; break;
        }
        //std::cerr << x1 << " " << x2 << " " << x3 << " " << x4 << " " << int(res) << " " << res << std::endl;
        return res;
    }

    std::string decode_str(std::string &s) {
        char r[s.length() / 5 + 1];
        int k = 0;
        for (size_t i = 0; i < s.length() - 4; i += 5) {
            r[k++] = decode_smile(s[i], s[i + 1], s[i + 2], s[i + 3]);
            if (k % 16 == 0) {
                ++i;
            }
        }
        r[k] = '\0';
        return r;
    }
    
    Executer::Cmd::Cmd(const std::string &s) {
        if (s.length() == 1 && !('0' <= s[0] && s[0] <= '9')) {
            type = s[0];
        } else if (s == "if") {
            type = T_IF;
        } else if (s == "$$") {
            type = T_2BUCKS;
        } else if (s == "goto") {
            type = T_GOTO;
        } else if (s == "mark") {
            type = T_MARK;
        } else if (s == "bite_up") {
            type = T_BITE_UP;
        } else if (s == "bite_down") {
            type = T_BITE_DOWN;
        } else if (s == "bite_left") {
            type = T_BITE_LEFT;
        } else if (s == "bite_right") {
            type = T_BITE_RIGHT;
        } else if (s == "move_up") {
            type = T_MOVE_UP;
        } else if (s == "move_down") {
            type = T_MOVE_DOWN;
        } else if (s == "move_left") {
            type = T_MOVE_LEFT;
        } else if (s == "move_right") {
            type = T_MOVE_RIGHT;
        } else if (s == "put_smell") {
            type = T_PUT_SMELL;
        } else if (s == "put") {
            type = T_PUT;
        } else if (s == "get") {
            type = T_GET;
        } else if (s == "==") {
            type = T_EQUAL;
        } else if (s[0] == '%') {
            type = 'v';
            strval = s.substr(1, s.length() - 1);
        } else {
            type = 'v';
            std::stringstream ss(s);
            ss >> intval;
        }
    }
    
    Executer::Cmd::Cmd(int, const std::string &s) {
        is_str_ptr = 1;
        type = 'v';
        strval = s;
    }
    
    Executer::Cmd::Cmd(int k) {
        is_str_ptr = 0;
        type = 'v';
        intval = k;
    }
    
    Executer::Executer(const std::string &filename) {
        std::ifstream ifs(filename);
        std::string content( (std::istreambuf_iterator<char>(ifs) ),
                             (std::istreambuf_iterator<char>()    ) );
        std::stringstream ss(decode_str(content));
        std::string t;
        while (std::getline(ss, t)) {
            if (t[0] == ':') {
                labels[t.substr(1, t.length() - 1)] = cmds.size() - 1;
            } else {
                cmds.push_back(Cmd(t));
            }
        }
        ifs.close();
    }

    int Executer::getSpecialParam(const std::string &name, const antlogic::Ant &ant) {
        if (name == "hasFood") {
            return ant.hasFood();
        } else {
            throw std::runtime_error("no such param: " + name);
        }
    }

    int Executer::getSensorVal(int x, int y, const std::string &name, antlogic::AntSensor sensors[3][3]) {
        if (name == "isWall") {
            return sensors[x][y].isWall;
        } else if (name == "isMyHill") {
            return sensors[x][y].isMyHill;
        } else if (name == "isEnemyHill") {
            return sensors[x][y].isEnemyHill;
        } else if (name == "isFriend") {
            return sensors[x][y].isFriend;
        } else if (name == "isEnemy") {
            return sensors[x][y].isEnemy;
        } else if (name == "isFood") {
            return sensors[x][y].isFood;
        } else if (name == "smell") {
            return sensors[x][y].smell;
        } else if (name == "smellIntensity") {
            return sensors[x][y].smellIntensity;
        } else {
            throw std::runtime_error("no such param: " + name);
        }
    }

    AntAction Executer::GetAction(const antlogic::Ant &ant, antlogic::AntSensor sensors[3][3]) { 
        // POLIZ
        AntAction result;
        std::stack<Cmd> s;
        std::unordered_map<std::string, int> vars;
        char *memory = ant.getMemory();
        for (size_t i = 0; i < cmds.size(); ++i) {
            int a, b, x;
            std::string lname, z;
            Cmd c(0);
            switch (cmds[i].type) {
            case 'v':
                s.push(cmds[i]);
                break;
            case '+':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a + b));
                break;
            case '-':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a - b));
                break;
            case '*':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a * b));
                break;
            case '/':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a / b));
                break;
            case '%':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a % b));
                break;
            case '|':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a | b));
                break;
            case '&':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a & b));
                break;
            case '<':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a < b));
                break;
            case '>':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a > b));
                break;
            case T_EQUAL:
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                s.push(Cmd(a == b));
                break;
            case '=':
                b = s.top().intval; s.pop();
                c = s.top(); s.pop();
                if (c.is_str_ptr) {
                    vars[c.strval] = b;
                } else {
                    memory[c.intval] = b;
                }
                s.push(b);
                break;
            case '$':
                z = s.top().strval;
                s.pop(), s.push(Cmd(0, z));
                break;
            case T_2BUCKS:
                x = s.top().intval;
                s.pop(), s.push(Cmd(x));
                break;
            case '^':
                c = s.top(); s.pop();
                if (c.is_str_ptr) {
                    x = vars[c.strval];
                } else {
                    x = memory[c.intval];
                }
                s.push(Cmd(x));
                break;
            case '#':
                b = s.top().intval; s.pop();
                a = s.top().intval; s.pop();
                x = getSensorVal(a, b, s.top().strval, sensors);
                s.pop(), s.push(Cmd(x));
                break;
            case '@':
                x = getSpecialParam(s.top().strval, ant);
                s.pop(), s.push(Cmd(x));
                break;
            case ';':
                s.pop();
                break;
            case 'n':
                x = s.top().intval; s.pop();
                s.push(-x);
                break;
            case '!':
                x = s.top().intval; s.pop();
                s.push(Cmd(!x));
                break;
            case T_GOTO:
                lname = s.top().strval; s.pop();
                i = labels[lname];
                break;
            case T_IF:
                lname = s.top().strval; s.pop();
                x = s.top().intval, s.pop();
                if (x) {
                    i = labels[lname];
                }
                break;
            case T_MARK:
                x = s.top().intval; s.pop();
                result.putSmell = true;
                result.smell = x;
                break;
            case T_PUT_SMELL:
                result.smell = s.top().intval;
                result.putSmell = true;
            case T_BITE_UP:
                result.actionType = antlogic::BITE_UP;
                return result;
            case T_BITE_DOWN:
                result.actionType = antlogic::BITE_DOWN;
                return result;
            case T_BITE_LEFT:
                result.actionType = antlogic::BITE_LEFT;
                return result;
            case T_BITE_RIGHT:
                result.actionType = antlogic::BITE_RIGHT;
                return result;
            case T_MOVE_UP:
                result.actionType = antlogic::MOVE_UP;
                return result;
            case T_MOVE_DOWN:
                result.actionType = antlogic::MOVE_DOWN;
                return result;
            case T_MOVE_LEFT:
                result.actionType = antlogic::MOVE_LEFT;
                return result;
            case T_MOVE_RIGHT:
                result.actionType = antlogic::MOVE_RIGHT;
                return result;
            case T_PUT:
                result.actionType = antlogic::PUT;
                return result;
            case T_GET:
                result.actionType = antlogic::GET;
                return result;
            default:
                std::cerr << "WTF?! " << cmds[i].type << std::endl;
            }
        }
        return result;
    }
}
