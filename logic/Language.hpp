#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stack>
#include <unordered_map>
#include <stdexcept>

#include "IAntLogicImpl.hpp"

namespace antlogic {
    struct Executer : public IAntLogic {

        enum {
            T_IF = 256,
            T_GOTO = 257,
            T_2BUCKS = 258,
            T_MARK = 259,
            T_BITE_UP = 260,
            T_BITE_DOWN = 261,
            T_BITE_LEFT = 262,
            T_BITE_RIGHT = 263,
            T_MOVE_UP = 264,
            T_MOVE_DOWN = 265,
            T_MOVE_LEFT = 266,
            T_MOVE_RIGHT = 267,
            T_PUT = 268,
            T_GET = 269,
            T_EQUAL = 270,
            T_PUT_SMELL = 271
        };
        
        struct Cmd {
            bool is_str_ptr;
            int type;
            int intval;
            std::string strval;
            Cmd(const std::string &s);
            Cmd(int, const std::string &s);
            Cmd(int k);
            Cmd &operator = (const Cmd &x) {
                is_str_ptr = x.is_str_ptr;
                type = x.type;
                intval = x.intval;
                strval = x.strval;
                return *this;
            }
        };

        std::vector<Cmd> cmds;
        std::unordered_map<std::string, int> labels;

        Executer(const std::string &filename);
        int getSpecialParam(const std::string &name, const Ant &ant);        
        int getSensorVal(int x, int y, const std::string &name, AntSensor sensors[3][3]);        
        AntAction GetAction(const antlogic::Ant &ant, antlogic::AntSensor sensors[3][3]);
    };
}
