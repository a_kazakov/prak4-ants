#include "Ant.hpp"

namespace antmanager {
    /*
        static int id_count;
        int global_id;
        int team_id;
        int skipping_left;
        int y, x;
        unsigned char memory[32];
        bool has_food;
        mutable antgui::Ant *tmp_gui;
    */
     
    Ant::Ant(int team_id_, int y_, int x_) :
        team_id(team_id_),
        skipping_left(0),
        y(y_), x(x_),
        has_food(0),
        tmp_gui(nullptr),
        global_id(id_count++)
    {
        std::memset(memory, 0, 32);
    }
    Ant::Ant(const Ant &a) {
        *this = a;
    }
    Ant::~Ant() {
        if (tmp_gui != nullptr) {
            //delete tmp_gui;
        }
    }
    Ant &Ant::operator = (const Ant &a) {
        global_id = a.global_id;
        team_id = a.team_id;
        skipping_left = a.skipping_left;
        y = a.y, x = a.x;
        std::memcpy(memory, a.memory, 32);
        has_food = a.has_food;
        tmp_gui = nullptr;
        return *this;
    }
    void Ant::bite() {
        skipping_left = 8;
    }
    bool Ant::isFrozen() const {
        return skipping_left > 0;
    }
    int Ant::frozenFor() const {
        return skipping_left;
    }
    void Ant::move(int y_, int x_) {
        y = y_;
        x = x_;
    }
    void Ant::step() {
        if (skipping_left > 0) {
            --skipping_left;
        }
    }
    bool Ant::takeFood() {
        bool res = has_food == 0;
        has_food = 1;
        return res;
    }
    bool Ant::dropFood() {
        bool res = has_food == 1;
        has_food = 0;
        return res;
    }
    char* Ant::getMemory() const {
        return memory;
    }
    bool Ant::hasFood() const {
        return has_food;
    }
    int Ant::getTeamId() const {
        return team_id;
    }
    antgui::Point Ant::getPoint() const {
        return antgui::Point(x, y);
    }
    std::shared_ptr<antgui::Ant> Ant::getGui() const {
        if (tmp_gui != nullptr) {
            //delete tmp_gui;
        }
        tmp_gui = new antgui::ConcreteAnt(hasFood(), isFrozen(), getPoint(), getTeamId());
        return std::shared_ptr<antgui::Ant>(tmp_gui);
    }
    antlogic::Ant *Ant::getLogic() {
        return dynamic_cast<antlogic::Ant *>(this);
    }
    bool Ant::operator < (const Ant &a) const {
        return global_id < a.global_id;
    }

}