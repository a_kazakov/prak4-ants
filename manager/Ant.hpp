#pragma once

#include <cstring>

#include "IAntGUI.hpp"
#include "IAntGuiImpl.hpp"
#include "IAntLogic.hpp"

#include "types.hpp"

namespace antmanager {
    class Ant : public antlogic::Ant {
        int team_id;
        int skipping_left;
        int y, x;
        mutable char memory[32];
        bool has_food;
        mutable antgui::Ant *tmp_gui;
    public:
        static int id_count;
        int global_id;
        Ant(int, int, int);
        Ant(const Ant &);
        virtual ~Ant();
        Ant &operator = (const Ant &);
        void bite();
        bool isFrozen() const;
        int frozenFor() const;
        void move(int, int);
        void step();
        bool takeFood();
        bool dropFood();
        virtual char* getMemory() const;
        virtual bool hasFood() const;
        virtual int getTeamId() const;
        virtual antgui::Point getPoint() const;
        std::shared_ptr<antgui::Ant> getGui() const;
        antlogic::Ant *getLogic();
        bool operator < (const Ant &) const;
    };
}
