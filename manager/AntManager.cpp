#include "AntManager.hpp"
#include <memory.h>

//
//  AntManager
//

namespace antmanager {

    int Ant::id_count = 0;

    void AntManager::addAnt(int team, int y, int x) {
        ants.push_back(new Ant(team, y, x));
        teams[team].push_back(ants[ants.size() - 1]->global_id);
        field(y, x).ants.insert(ants[ants.size() - 1]);
    }

    void AntManager::moveAnt(Ant *antptr, Direction dir) {
        antgui::Point pos = antptr->getPoint();
        int py = pos.y;
        int px = pos.x;
        int ny = py, nx = px;
        switch (dir) {
        case UP:
            --ny;
            break;
        case DOWN:
            ++ny;
            break;
        case RIGHT:
            ++nx;
            break;
        case LEFT:
            --nx;
            break;
        default:
            break;
        }
        if (field.isInside(ny, nx)) {
            antptr->move(ny, nx);
            field(py, px).ants.erase(antptr);
            field(ny, nx).ants.insert(antptr);
        }
    }
    
    void AntManager::biteAnt(Ant *antptr, Direction dir) {
        antgui::Point pos = antptr->getPoint();
        int py = pos.y;
        int px = pos.x;
        int ny = py, nx = px;
        switch (dir) {
        case UP:
            --ny;
            break;
        case DOWN:
            ++ny;
            break;
        case RIGHT:
            ++nx;
            break;
        case LEFT:
            --nx;
            break;
        default:
            break;
        }
        if (field.isInside(ny, nx)) {
            std::set<Ant *> &ants = field(ny, nx).ants;
            if (ants.empty()) {
                return;
            }
            Ant *res = nullptr;
            std::for_each(ants.begin(), ants.end(), [=, &res](Ant *a) {
                if (a->getTeamId() != antptr->getTeamId()) {
                    if (!res || a->frozenFor() < res->frozenFor()) {
                        res = a;
                    }
                }
            });
            if (res) {
                res->bite();
                dropFood(res);
            }
        }
    }
    
    void AntManager::takeFood(Ant *antptr) {
        antgui::Point pos = antptr->getPoint();
        int py = pos.y;
        int px = pos.x;
        if (field(py, px).getFood() > 0 && antptr->takeFood()) {
            field(py, px).takeFood();
        }
    }
    
    void AntManager::dropFood(Ant *antptr) {
        antgui::Point pos = antptr->getPoint();
        int py = pos.y;
        int px = pos.x;
        if (antptr->dropFood()) {
            field(py, px).putFood(1);
        }
    }
    
    // ================================================
    
    AntManager::AntManager(int height, int width, int teamCount, int maxAntCountPerTeam) : 
        field(height, width)
    {
        this->height = height;
        this->width = width;
        this->teamCount = teamCount;
        this->maxAntCountPerTeam = maxAntCountPerTeam;
        teams.resize(teamCount);
    }
    
    AntManager::~AntManager() {
        std::for_each(ants.begin(), ants.end(), [](Ant *a) {
            delete a;
        });
    }

    void AntManager::step(int step) {
        const int init_pos[4][2] = {{0, 0}, {0, width - 1}, {height - 1, 0}, {height - 1, width - 1}};
        if (!is_inited) {
            while (food_it != food_iterator()) {
                auto f = *food_it;
                field(f->getPoint()).putFood(f->getCount());
                food_it++;
            }
            for (int i = 0; i < teamCount; ++i) {
                field(init_pos[i][0], init_pos[i][1])
                    .putFood(-field(init_pos[i][0], init_pos[i][1]).getFood());
            }
            is_inited = true;
        }
        
        for (int i = 0; i < teamCount; ++i) {
            if (step % 10 == 0) {
                if (teams[i].size() < static_cast<unsigned int>(maxAntCountPerTeam)) {
                    addAnt(i, init_pos[i][0], init_pos[i][1]);
                }
            }
        }
        
        for(int team = 0; team < teamCount; ++team) {
            for (size_t i = 0; i < teams[team].size(); ++i) {
                Ant *antptr = ants[teams[team][i]];
                if (!antptr->isFrozen()) {
                    antgui::Point pos = antptr->getPoint();
                    int py = pos.y;
                    int px = pos.x;                   
                    std::shared_ptr<antlogic::IAntLogic> logic = 
                                            antlogic::IAntLogic::GetAntLogic(antptr->getTeamId());
                    // fill sensors
                    antlogic::AntSensor sensor[3][3];
                    for (int dy = -1; dy <= 1; ++dy) {
                        for (int dx = -1; dx <= 1; ++dx) {
                            int cy = py + dy;
                            int cx = px + dx;
                            antlogic::AntSensor &cur_scell = sensor[dx + 1][dy + 1];
                            if (!field.isInside(cy, cx)) {
                                cur_scell.isWall = true;
                            } else {
                                FieldCell &cur_fcell = field(cy, cx);
                                cur_scell.isWall = false;
                                cur_scell.isFood = cur_fcell.getFood() > 0;
                                cur_scell.isMyHill =  cy == init_pos[team][0]
                                                   && cx == init_pos[team][1];
                                cur_scell.isEnemyHill = false;
                                if (!cur_scell.isMyHill) {
                                    for (int t = 0; t < teamCount; ++t) {
                                        if (cy == init_pos[t][0] && cx == init_pos[t][1]) {
                                            cur_scell.isEnemyHill = true;
                                            break;
                                        }
                                    }
                                }
                                cur_scell.isFriend = false;
                                cur_scell.isEnemy  = false;
                                std::for_each(cur_fcell.ants.begin(), cur_fcell.ants.end(), 
                                              [=, &cur_scell](Ant *a) {
                                    if (a->global_id != antptr->global_id) {
                                        if (a->getTeamId() == antptr->getTeamId()) {
                                            cur_scell.isFriend = true;
                                        } else {
                                            cur_scell.isEnemy = true;
                                        }
                                    }
                                });
                                cur_scell.smell = cur_fcell.getSmell();
                                cur_scell.smellIntensity = cur_fcell.getSmellIntensity();
                            }
                        }
                    }
                    if (!antptr->isFrozen()) {
                        antlogic::AntAction act = logic->GetAction(*antptr->getLogic(), sensor);
                        if (act.putSmell) {
                            field(py, px).mark(act.smell);
                        }
                        switch (act.actionType) {
                        case antlogic::AntActionType::MOVE_UP:
                            moveAnt(antptr, UP);
                            break;
                        case antlogic::AntActionType::MOVE_RIGHT:
                            moveAnt(antptr, RIGHT);
                            break;
                        case antlogic::AntActionType::MOVE_DOWN:
                            moveAnt(antptr, DOWN);
                            break;
                        case antlogic::AntActionType::MOVE_LEFT:
                            moveAnt(antptr, LEFT);
                            break;
                        case antlogic::AntActionType::BITE_UP:
                            biteAnt(antptr, UP);
                            break;
                        case antlogic::AntActionType::BITE_RIGHT:
                            biteAnt(antptr, RIGHT);
                            break;
                        case antlogic::AntActionType::BITE_DOWN:
                            biteAnt(antptr, DOWN);
                            break;
                        case antlogic::AntActionType::BITE_LEFT:
                            biteAnt(antptr, LEFT);
                            break;
                        case antlogic::AntActionType::GET:
                            takeFood(antptr);
                            break;
                        case antlogic::AntActionType::PUT:
                            dropFood(antptr);
                            break;
                        }
                    }
                }
                antptr->step();
            }
        } // khalman.m@gmail.com
        
        for (int i = 0; i < field.getHeight(); ++i) {
            for (int j = 0; j < field.getWidth(); ++j) {
                field(i, j).step();
            }
        }
        
        gui->BeginPaint();

        for (int i = 0; i < field.getHeight(); ++i) {
            for (int j = 0; j < field.getWidth(); ++j) {
                gui->SetFood(antgui::ConcreteFood(antgui::Point(j, i), field(i, j).getFood()));
                std::for_each(field(i, j).ants.begin(), field(i, j).ants.end(), 
                                                    [=](Ant *a) {
                    gui->SetAnt(*a->getGui());
                });
            }
        }

        gui->EndPaint();
               
        for (int t = 0; t < teamCount; t++) {
            gui->SetTeamScore(t, field(init_pos[t][0], init_pos[t][1]).getFood());
        }
    }

    void AntManager::setGui(std::shared_ptr<antgui::IAntGui> gui) {
        this->gui = gui;
    }

    void AntManager::setFoodGeneretor(std::shared_ptr<food_iterator> it) {
        food_it = *it;
        is_inited = false;
    }
}
