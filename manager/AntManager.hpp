#ifndef ANTMANAGER_HPP
#define ANTMANAGER_HPP

#include "IAntGUI.hpp"
#include "IAntLogic.hpp"

#include "Matrix.hpp"
#include "FieldCell.hpp"
#include "Ant.hpp"
#include "food_iterator.hpp"

#include <memory>

namespace antmanager {

    class AntManager {
        int maxAntCountPerTeam;
        int teamCount;
        int height;
        int width;
        std::shared_ptr<antgui::IAntGui> gui;
        Matrix<FieldCell> field;
        bool is_inited;
        food_iterator food_it;
        std::vector<std::vector<int>> teams;
        std::vector<Ant *> ants;
        void addAnt(int team, int y, int x);
        void moveAnt(Ant *antptr, Direction dir);
        void biteAnt(Ant *antptr, Direction dir);
        void takeFood(Ant *antptr);
        void dropFood(Ant *antptr);
    public:
        AntManager(int height,
                int width,
                int teamCount,
                int maxAntCountPerTeam);
        ~AntManager();
        void step(int iRun);
        void setGui(std::shared_ptr<antgui::IAntGui> gui);
        void setFoodGeneretor(std::shared_ptr<food_iterator> it);
    };
    
}

#endif
