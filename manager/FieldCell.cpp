#include "FieldCell.hpp"

namespace antmanager {
    /*
        std::unordered_set<Ant> ants;
        int smell, smell_intensity;
    */
    FieldCell::FieldCell() : smell(0), smell_intensity(0), food(0), ants() {};
    
    void FieldCell::mark(int type) {
        smell = type;
        smell_intensity = 100;
    }
    
    int FieldCell::getSmell() const {
        return smell_intensity > 0 ? smell : 0;
    }

    int FieldCell::getSmellIntensity() const {
        return smell_intensity;
    }

    void FieldCell::step() {
        if (smell_intensity > 0) {
            --smell_intensity;
        }
    }

    int FieldCell::getFood() const {
        return food;
    }
    
    bool FieldCell::takeFood() {
        if (food > 0) {
            --food;
            return true;
        }
        return false;
    }
    
    void FieldCell::putFood(int amount) {
        //std::cerr << "Put food!" << std::endl;
        food += amount;
    }

}
