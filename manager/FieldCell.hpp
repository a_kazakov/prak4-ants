#pragma once

#include <set>
#include <utility>

#include "types.hpp"
#include "Ant.hpp"

namespace antmanager {
    class FieldCell {
        int smell, smell_intensity;
        int food;
    public:
        std::set<Ant *> ants;
        FieldCell();
        void mark(int);
        int getSmell() const;
        int getSmellIntensity() const;
        void step();
        int getFood() const;
        bool takeFood();
        void putFood(int);
    };
}
