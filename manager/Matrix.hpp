#pragma once

#include <cstring>

#include "IAntGuiImpl.hpp"

namespace antmanager {
    template <typename T>
    class Matrix {
        int width;
        int height;
        T *data;
    public:
        Matrix(int w = 0, int h = 0) : width(w), height(h) {
            data = new T[width * height];
        }
        Matrix(const Matrix &a) : data(nullptr) {
            *this = a;
        }
        Matrix &operator = (const Matrix &a) {
            width = a.width;
            height = a.height;
            if (data != nullptr) {
                delete [] data;
            }
            data = new T[width * height];
            std::memcpy(data, a.data, width * height * sizeof(T));
            return *this;
        }            
        ~Matrix() {
            delete [] data;
        }
        const T &operator() (const antgui::Point &p) const {
            return data[width * p.y + p.x];
        }
        T &operator() (const antgui::Point &p) {
            return data[width * p.y + p.x];
        }
        const T &operator() (int y, int x) const {
            return data[width * y + x];
        }
        T &operator() (int y, int x) {
            return data[width * y + x];
        }
        int getWidth() const {
            return width;
        }
        int getHeight() const {
            return height;
        }
        bool isInside(int y, int x) const {
            return 0 <= y && y < height && 0 <= x && x < width;
        }
    };
}
