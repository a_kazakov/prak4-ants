#pragma once 

#include "IAntGUI.hpp"
#include "IAntGuiImpl.hpp"
#include "IAntLogic.hpp"

#include "Matrix.hpp"

namespace antmanager {

    class food_iterator: public std::iterator< std::input_iterator_tag, antgui::Food> {
        int x;
        Matrix<int> field;
        mutable antgui::Food *tmp_food;
        
        void fill() {
            static const int iter = 10;
            static const int mx  = 50;
            float zx, zy, cx, cy;
            for (int i = 0; i < field.getHeight(); ++i) {
                for (int j = 0; j < field.getWidth(); ++j) 
                {
                    int y = (j - field.getWidth() / 2);
                    int x = (i - field.getHeight() / 2);
                    zx = y * 0.1;
                    zy = x * 0.1;
                    cx = 0.13;
                    cy = -0.67;
                    int k;
                    for (k = 0; zx * zx + zy * zy < mx && k < iter; ++k) {
                        float tx = zx;
                        float ty = zy;
                        // z^2 + c
                        zx = tx * tx - ty * ty + cx;
                        zy = 2 * tx * ty + cy;
                    }
                    field(i, j) = 300 * sqrt(std::max(0.0, 1 - 2 * sqrt(y * y + x * x) / field.getWidth())) * sqrt(k % iter);
                }
            }
        }
    public:
        food_iterator(int size = 0) : field(size, size), tmp_food(nullptr) {
            if (size != 0) {
                fill();
            }
        }        
        food_iterator(const food_iterator &p) : x(p.x), field(p.field), tmp_food(nullptr) {}
        
        ~food_iterator() {
            if (tmp_food != nullptr) {
                delete tmp_food;
            }
        }
        
        food_iterator &operator = (const food_iterator &p) {
            x = p.x;
            field = p.field;
            tmp_food = nullptr;
            return *this;
        }
        
        food_iterator& operator++() {
            ++x;
            return *this;
        }
        
        food_iterator operator++(int) {
            food_iterator tmp(*this);
            operator++();
            return tmp;
        }
        
        friend bool operator == (const food_iterator &a, const food_iterator &b) {
            return
                (a.field.getWidth() == b.field.getWidth() &&
                 a.field.getHeight() == b.field.getHeight() && a.x == b.x) ||
                (a.x == a.field.getWidth() * a.field.getHeight() && b.field.getWidth() == 0) ||
                (b.x == b.field.getWidth() * b.field.getHeight() && a.field.getWidth() == 0);
        }
        
        friend bool operator != (const food_iterator &a, const food_iterator &b) {
            return !(a == b);
        }

        std::shared_ptr<antgui::Food> operator *() {
            int i = x / field.getWidth();
            int j = x % field.getWidth();
            if (tmp_food != nullptr) {
                delete tmp_food;
            }
            tmp_food = new antgui::ConcreteFood(antgui::Point(j, i), field(i, j));
            return std::shared_ptr<antgui::Food> (new antgui::ConcreteFood(antgui::Point(j, i), field(i, j)));
        }
    };
}
